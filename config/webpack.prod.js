const merge = require('webpack-merge')
const base = require('./webpack.base')

const CleanWebpack = require('clean-webpack-plugin')

module.exports = merge(base, {
  mode: 'production',
  output: {
    filename: '[name].[chunkhash].js'
  },
  plugins: [
    new CleanWebpack()
  ]
})
